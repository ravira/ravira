const fs = require('fs');
const axios = require('axios');

const replacementTags = [
    'START_SECTION:random_quote',
    // 'START_SECTION:another_section',
    // Add more tags as needed
];

// Function to fetch a random quote from the API
async function fetchRandomQuote() {
    try {
        const response = await axios.get('https://api.quotable.io/quotes/random');
        return `# Do read a quote before you go \n\n${response.data[0].content}`;
    } catch (error) {
        console.error('Error fetching quote:', error);
        return null;
    }
}

// Function to generate new content for a replacement tag
async function fetchNewContentForTag(tag) {
    // Here you can implement logic to fetch or generate new content for the specified tag
    switch (tag) {
        case 'START_SECTION:random_quote':
            return await fetchRandomQuote();
        case 'START_SECTION:another_section':
            return 'This is the content for another section.';
        // Add more cases as needed
        default:
            return ''; // Return empty string if new content cannot be fetched/generated
    }
}

// Function to replace content between comment tags
function replaceContentBetweenTags(data, startTag, endTag, newContent) {
    // Regular expression to match the content between the comment tags
    const regex = new RegExp(`<!--${startTag}-->([\\s\\S]*?)<!--${endTag}-->`);

    // Check if the comment tags exist
    const match = data.match(regex);
    if (match && match[1]) {
        // Content found between the comment tags
        const oldContent = match[1].trim();

        // Replace the old content with the new content
        const replacedData = data.replace(
            regex,
            `<!--${startTag}-->\n${newContent}\n<!--${endTag}-->`
        );

        return replacedData;
    } else {
        // If the comment tags are not present, log a message
        console.log(`Comment tags ${startTag} and ${endTag} not found. No replacement needed.`);
        return data;
    }
}


// Function to read content from README.md file
async function readFromFile() {
    try {
        const content = fs.readFileSync('README.md', 'utf8');
        return content;
    } catch (error) {
        console.error('Error reading file:', error);
        return null;
    }
}

// Function to write content to README.md file
async function writeToFile(quote) {
    try {
        const existingContent = await readFromFile();
        if (existingContent !== null) {
            let updatedContent = existingContent;

            for (const tag of replacementTags) {
                // Fetch or generate new content for the current tag
                const newContent = await fetchNewContentForTag(tag);
        
                // Perform the replacement in memory
                updatedContent = replaceContentBetweenTags(updatedContent, tag, `END_SECTION:${tag.split(':')[1]}`, newContent);
            }
        
        
            //`${existingContent}\n\n# Read a quote before you go\n\n${quote}`;
            fs.writeFileSync('README.md', updatedContent, { flag: 'w' });
            console.log('File updated successfully');
        }
    } catch (error) {
        console.error('Error writing to file:', error);
    }
}

// Execute the process to fetch a quote and append it to the file
async function main() {
    const quote = await fetchRandomQuote();
    if (quote) {
        await writeToFile(quote);
    } else {
        console.log('No quote fetched');
    }
}

// Start the process
main();